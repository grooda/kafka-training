package com.ddt.das.ea.consumer;

import com.ddt.das.ea.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

import static java.time.Duration.ofSeconds;

/**
 * @author rich
 */
@Slf4j
public class BasicConsumer {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "group2"); // ConsumerGroup
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("auto.offset.reset", "earliest");

        String topicName = "test";
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(topicName));

        try {
            log.info("Start listen incoming messages ...");

            while (true) {
                // poll records from Kafka
                ConsumerRecords<String, String> records = consumer.poll(ofSeconds(5));

                for (ConsumerRecord<String, String> record : records){
                    CommonUtil.printRecord(record);
                }
            }
        } finally {
            consumer.close();
            log.info("Stop listen incoming messages");
        }
    }
}
