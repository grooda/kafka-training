package com.ddt.das.ea.consumer;

import com.ddt.das.ea.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.record.TimestampType;

import java.time.Duration;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author rich
 */
@Slf4j
public class CommitSpecifiedConsumer {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "group1");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("auto.offset.reset", "earliest");
        props.put("enable.auto.commit", "false");


        String topicName = "test";
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(topicName));

        // TopicPartition & Offset collection
        Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();

        int recordCount = 0;
        try {
            log.info("Start listen incoming messages ...");

            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                for (ConsumerRecord<String, String> record : records) {
                    String topic = record.topic();
                    int partition = record.partition();
                    long offset = record.offset();
                    CommonUtil.printRecord(record);

                    currentOffsets.put(new TopicPartition(topic, partition), new OffsetAndMetadata(offset + 1, ""));

                    // async commit offset
                    Map<TopicPartition, OffsetAndMetadata> offsetToCommits = copyHashmap(currentOffsets);
                    consumer.commitAsync(offsetToCommits, null);
                    currentOffsets.clear();
                    recordCount++;
                }
            }
        } catch(Exception e) {
            log.error("Unexpected error", e);
        } finally {
            try {
                consumer.commitSync(currentOffsets);
            } finally {
                consumer.close();
            }
            log.info("Stop listen incoming messages");
        }
    }

    private static Map<TopicPartition, OffsetAndMetadata> copyHashmap(Map<TopicPartition, OffsetAndMetadata> origin) {
        Map<TopicPartition, OffsetAndMetadata> copy = new HashMap<>();
        for(Map.Entry<TopicPartition, OffsetAndMetadata> entry : origin.entrySet()) {
            copy.put(entry.getKey(), entry.getValue());
        }
        return copy;
    }
}
