package com.ddt.das.ea.consumer;

import com.ddt.das.ea.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static java.time.Duration.ofSeconds;
import static java.util.Collections.singleton;
import static org.apache.kafka.clients.consumer.ConsumerConfig.*;

/**
 * @author rich
 */
@Slf4j
public class TransactionAwareConsumer {

    private static final String CONSUMER_GROUP_ID = "group-1";
    private static final String INPUT_TOPIC = "input";

    public static void main(String[] args) {
        KafkaConsumer<String, String> consumer = createKafkaConsumer();

        try {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(ofSeconds(10));
                if(records.isEmpty()) {
                    continue;
                }

                for (ConsumerRecord<String, String> record : records){
                    CommonUtil.printRecord(record);
                }

                Map<TopicPartition, OffsetAndMetadata> offsetsToCommit = new HashMap<>();
                for (TopicPartition partition : records.partitions()) {
                    List<ConsumerRecord<String, String>> partitionedRecords = records.records(partition);
                    long offset = partitionedRecords.get(partitionedRecords.size() - 1).offset();
                    log.info("offset: {}", offset);
                    offsetsToCommit.put(partition, new OffsetAndMetadata(offset + 1));
                }

                consumer.commitAsync(offsetsToCommit, (offsets, exception) -> {
                    if (exception != null) {
                        // retry commitAsync
                        consumer.commitAsync(null);
                    } else {
                        log.info("record committed successfully!");
                    }
                });
            }
        } catch (KafkaException e) {
            log.error("consume data error!", e);
        }

    }

    private static KafkaConsumer<String, String> createKafkaConsumer() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(GROUP_ID_CONFIG, CONSUMER_GROUP_ID);
        props.put(ENABLE_AUTO_COMMIT_CONFIG, "false");

        // In addition to reading messages that are not part of a transaction,
        // also be able to read ones that are, after the transaction is committed.
        props.put(ISOLATION_LEVEL_CONFIG, "read_committed");
        props.put(KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(singleton(INPUT_TOPIC));
        return consumer;
    }
}
