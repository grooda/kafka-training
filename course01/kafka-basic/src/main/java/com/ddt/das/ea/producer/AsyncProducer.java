package com.ddt.das.ea.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;

import java.util.Properties;

/**
 * Sending a Message Asynchronously
 * @author rich
 */
@Slf4j
public class AsyncProducer {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.1.2:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("acks","all");
        props.put("max.in.flight.requests.per.connection","1");
        props.put("retries", Integer.MAX_VALUE+"");

        try (Producer<String, String> producer = new KafkaProducer<>(props)) {

            String topicName = "test";
            int msgCount = 1000;

            try {
                log.info("Start sending messages ...");

                for (int i = 0; i < msgCount; i++) {
                    ProducerRecord record = new ProducerRecord<>(topicName, "" + i, "msg_" + i);

                    // send method with a callback function
                    producer.send(record, new Callback() {
                        @Override
                        public void onCompletion(RecordMetadata metadata, Exception e) {
                            log.info("Sent data to Topic: {}, Offset : {} in partition", metadata.topic(), metadata.offset(), metadata.partition());

                            if (e != null) {
                                log.error("Sent record failed, record: {}", record, e);
                                //TODO: retry or send to other topic
                            }
                        }
                    });
                }

            } catch (Exception e) {
                log.error("Unexpected error!", e);
            }
        }

        log.info("Message sending completed!");
    }

}


