package com.ddt.das.ea.consumer;

import com.ddt.das.ea.model.Device;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;

import static java.time.Duration.ofSeconds;

@Slf4j
public class JsonConsumer {

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "group1"); // ConsumerGroup
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "com.ddt.das.ea.deserializer.DeviceDeserializer");
        props.put("auto.offset.reset", "earliest");

        String topicName = "streaming.iot.device";
        Consumer<String, Device> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(topicName));

        try {
            log.info("Start listen incoming messages ...");

            while (true) {
                // poll records from Kafka
                ConsumerRecords<String, Device> records = consumer.poll(ofSeconds(5));

                for (ConsumerRecord<String, Device> record : records){
                    log.info("Device received: {}", record.value());
                }
            }
        } finally {
            consumer.close();
            log.info("Stop listen incoming messages");
        }
    }
}
