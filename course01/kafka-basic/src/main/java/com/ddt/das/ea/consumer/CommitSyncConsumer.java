package com.ddt.das.ea.consumer;

import com.ddt.das.ea.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.*;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

/**
 * @author rich
 */
@Slf4j
public class CommitSyncConsumer {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "group1");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("auto.offset.reset", "earliest");
        props.put("enable.auto.commit", "false"); // disable auto commit

        String topicName = "test";
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(topicName));

        try {
            log.info("Start listen incoming messages ...");
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));

                for (ConsumerRecord<String, String> record : records){
                    CommonUtil.printRecord(record);
                }

                try {
                    // Commit last poll offset
                    consumer.commitSync();
                } catch (CommitFailedException e) {
                    log.error("commit failed!", e);
                }
            }
        } finally {
            consumer.close();
            log.info("Stop listen incoming messages");
        }
    }
}
